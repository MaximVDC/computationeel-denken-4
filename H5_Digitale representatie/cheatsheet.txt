===================================================================================================================================
LIST SYNTAX
===================================================================================================================================
- lijst = []                            creëer een lege lijst
- lijst = [a,b,c,d]                     creëer een lijst met elementen
- lijst[2]                              geef het element met index 2 (tellen vanaf 0) == het derde element      bv. lijst[2] -> c

===================================================================================================================================
LIST FUNCTIONS
===================================================================================================================================

len(list)           geeft het aantal elementen in de lijst                              bv. len([1,3,5])            ->  3

list.remove(elem)   verwijder het eerste voorkomen van een element uit lijst            bv. [1,3,8,4].remove(8)     ->  [1,3,4]

list.append(elem)   voeg een element achteraan een lijst toe                            bv. [1,4,7].append(8)       ->  [1,4,7,8]

list.index(elem)    retourneert de index van een element in een lijst                   bv. [1,4,7].index(7)        ->  2

elem in list        kijkt of een element in een lijst zit, retourneert True of False    bv. 4 in [1,4,7]            ->  True

for elem in list:   overloopt elk element in een lijst                                  bv. for getal in getallenlijst:
                                                                                            for name in inschrijvingen:
                                                                                            ...

===================================================================================================================================
DICTIONARY SYNTAX
===================================================================================================================================

dict = {}                   creëer een lege dictionary

dict = {                    creëer een gevulde dictionary
    "naam": "Jan",
    5: [a,b,c],
}

dict["naam"]                geef de value die hoort bij key naam            bv. dict["naam"]    ->  "Jan"
                                                                                dict[5]         ->  [a,b,c]

dict[new_key] = new_value   voeg een nieuw element toe met een nieuwe key

===================================================================================================================================
DICTIONARY FUNCTIONS
===================================================================================================================================

dict.pop(key)               verwijder een element uit de dictionary met key  bv. dict.pop("naam")   -> {5: [a,b,c]}

len(dict)                   geeft het aantal elementen in de dictionary     bv. len({})             -> 0