
# TODO-1 Creëer hier een dictionary 'uitgangen' met alle Griekse uitgangen voor de 1ste en 2de klasse.
#  Je bepaalt zelf de structuur van deze dictionary. Toon deze aan de leerkracht ter controle.
uitgangen = {
    1: {
        "nom": {
            "m":{
                "enk": "ος",
                "mv": "οι"
            },
            "v":{
                "enk": "η",
                "mv": "αι"
            },
            "o":{
                "enk": "ον",
                "mv": "α"
            },
        },
        "acc": {
            "m": {
                "enk": "ον",
                "mv": "ους"
            },
            "v": {
                "enk": "ην",
                "mv": "ας"
            },
            "o": {
                "enk": "ον",
                "mv": "α"
            },
        },
        "gen": {
            "m": {
                "enk": "ου",
                "mv": "ων"
            },
            "v": {
                "enk": "ης",
                "mv": "ων"
            },
            "o": {
                "enk": "ου",
                "mv": "ων"
            },
        },
        "dat": {
            "m": {
                "enk": "ῳ",
                "mv": "οις"
            },
            "v": {
                "enk": "ῃ",
                "mv": "αις"
            },
            "o": {
                "enk": "ῳ",
                "mv": "οις"
            },
        },
    },
    2: {
        "nom": {
            "m": {
                "enk": "",
                "mv": "ες"
            },
            "v": {
                "enk": "",
                "mv": "ες"
            },
            "o": {
                "enk": "",
                "mv": "α"
            },
        },
        "acc": {
            "m": {
                "enk": "α",
                "mv": "ας"
            },
            "v": {
                "enk": "α",
                "mv": "ας"
            },
            "o": {
                "enk": "",
                "mv": "α"
            },
        },
        "gen": {
            "m": {
                "enk": "ος",
                "mv": "ων"
            },
            "v": {
                "enk": "ος",
                "mv": "ων"
            },
            "o": {
                "enk": "ος",
                "mv": "ων"
            },
        },
        "dat": {
            "m": {
                "enk": "ι",
                "mv": "σι"
            },
            "v": {
                "enk": "ι",
                "mv": "σι"
            },
            "o": {
                "enk": "ι",
                "mv": "σι"
            },
        },
    }
}