
import unittest
from unittest.mock import patch
import io
import main

class ConjugatorTest(unittest.TestCase):

    @patch('builtins.input', side_effect=['ῥητωρ', 'acc', 'mv'])
    def test_1_class_dat_mv(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.calculator()
            self.assertEqual(fake_out.getvalue(), "ῥητορας\n")

    @patch('builtins.input', side_effect=['γυνη', 'dat', 'mv'])
    def test_gyne_dat_mv(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.calculator()
            self.assertEqual(fake_out.getvalue(), "γυναιξι\n")

    @patch('builtins.input', side_effect=['σωμα', 'dat', 'mv'])
    def test_svma_dat_mv(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.calculator()
            self.assertEqual(fake_out.getvalue(), "σωμασι\n")

    @patch('builtins.input', side_effect=['λεων', 'dat', 'mv'])
    def test_leon_dat_mv(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.calculator()
            self.assertEqual(fake_out.getvalue(), "λεουσι\n")

    @patch('builtins.input', side_effect=['παιδισκη', 'gen', 'mv'])
    def test_paidiskh_gen_enk(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.calculator()
            self.assertEqual(fake_out.getvalue(), "παιδισκης\n")

    @patch('builtins.input', side_effect=['παιδισκη', 'dat', 'enk'])
    def test_paidiskh_dat_enk(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.calculator()
            self.assertEqual(fake_out.getvalue(), "παιδισκῃ\n")