from woordenlijst import woordenlijst
from uitgangen import uitgangen

# De opdracht hier is om een programma te schrijven dat Griekse substantieven verbuigt naargelang de invoer van de gebruiker.

# TODO-1: Vraag de gebruiker welk Grieks woord hij/zij wenst te verbuigen (grondwoord, geen lidwoord, geen accenten behalve spiritus!).
#  Gebruik de gekende afkortingen in dit programma (nom, acc, gen, dat, enk, mv, m, v, o)



# TODO-2 Vraag de gebruiker eerst welke naamval en daarna welk getal hij wenst.


# TODO-3 Schrijf de functie greek_conjugator(), die de logica van het programma bevat. Bekijk goed welke informatie er reeds beschikbaar is in de gegeven woordenlijst.
#  Schrijf een programma dat met alle beschikbare informatie (woordenlijst en uitgangen) de gevraagde vorm print.
#  Houd rekening met het gebrek aan uitgang in de nom enk in de tweede klasse en de uitzonderingen bij de datief meervoud in dezelfde klasse.


