# Schrijf een prograamma dat de scores van studenten omzet naar hun waardering.
# Op het einde zou je een nieuwe dictionary moeten hebben die de namen als keys heeft en de waardering als waarde.
#
# Verander de student_scores dictionary niet.
# Schrijf zelf geen print statements.
#
# Dit zijn de waarderingscriteria:
#
#     91-100 = "Schitterend"
#     81-90 = "Overstijgt de verwachtingen"
#     71-80 = "Aanvaardbaar"
#     70 of lager = "Gebuisd"

def hogwarts():
    student_scores = {
        "Harry": 81,
        "Ron": 78,
        "Hermione": 99,
        "Draco": 74,
        "Neville": 62,
    }
    # Verander de code hierboven niet

    # TODO-1: Creëeer een lege dictionary met de naam student_grades.


    # TODO-2: Schrijf hieronder je code om de aan student_grades toe te voegen.


    # Verander de code hieronder niet
    return student_grades
