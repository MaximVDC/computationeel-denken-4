
import unittest
from unittest.mock import patch
import io
import main


class HogwartsTest(unittest.TestCase):


    def test_hogwarts(self):
        self.assertEqual(main.hogwarts(),{"Harry": "Overstijgt de verwachtingen", "Ron": "Aanvaardbaar", "Hermione": "Schitterend", "Draco": "Aanvaardbaar", "Neville": "Gebuisd"})