
import unittest
from unittest.mock import patch
import io
import main

class CalculatorTest(unittest.TestCase):

    @patch('builtins.input', side_effect=['8', '+', '3', 'y', '*', '5','n'])
    def test_movie(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.calculator()
            self.assertEqual(fake_out.getvalue(), "+\n-\n*\n/\n8.0 + 3.0 = 11.0\n11.0 * 5.0 = 55.0\n")

    @patch('builtins.input', side_effect=['7', '/', '2', 'y', '*', '4','n'])
    def test_division_multiply(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.calculator()
            self.assertEqual(fake_out.getvalue(), "+\n-\n*\n/\n7.0 / 2.0 = 3.5\n3.5 * 4.0 = 14.0\n")

    @patch('builtins.input', side_effect=['8', '+', '3', 'y', '*', '5','y', '-', '22', 'n'])
    def test_3_operations(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.calculator()
            self.assertEqual(fake_out.getvalue(), "+\n-\n*\n/\n8.0 + 3.0 = 11.0\n11.0 * 5.0 = 55.0\n55.0 - 22.0 = 33.0\n")