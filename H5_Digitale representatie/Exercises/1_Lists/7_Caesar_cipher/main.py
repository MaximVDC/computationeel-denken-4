alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
            'v', 'w', 'x', 'y', 'z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
            'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

# Het Caesarcijfer is een klassieke substitutieversleuteling.
# Het cijfer, de versleuteling, is naar Julius Caesar genoemd, die het geheimschrift gebruikte.
# De versleuteling werkt door alle letters van het te versturen bericht, van de klare tekst,
# door een vooraf vastgestelde verschuiving te vervangen, steeds op dezelfde manier.
# Bij verschuiving van drie, wordt de letter A door de letter D vervangen.
# Om de code te ontcijferen moet je dezelfde verschuiving toepassen in de tegenovergestelde richting.

#TODO-1: Schrijf een functie genaamd 'caesar' die 'text' en 'shift' als inputs krijgt.

# gebruik onderstaande code
text = input("Typ je bericht:\n").lower()
shift = int(input("Typ het verschuivingsgetal:\n"))

#TODO-2: Binnen de 'caesar' functie, verschuif je elke letter van 'text' vooruit in in het alfabet met de shift waarde en je print het versleutelde resultaat.
#bvb.
#plain_text = "hello"
#shift = 5
#cipher_text = "mjqqt"
#print output: "The encoded text is mjqqt"

##HINT: de index van een item in een lijst kan je vinden met de functie index(), bvb. lijst.index(item)

##🐛Bug alert: Wat gebeurt er als je de letters verschuift voorbij de letter z?🐛



#TODO-3: aanroep de caeser functie en voer je inputs in. Je zou nu een gecodeerd bericht moeten hebben. Test dit uit met volgende invoer:
# direction: encode
# text: zand
# shift: 2
# het resultaat zou moeten zijn: bcpf




# TODO-4: pas de caesar code aan zodat je ook een decryptie (in de omgekeerde richting) kan uitvoeren wanneer je de versleutelde tekst
#  invoert samen met de originele shift waarde. Je kan je code testen als volgt:
#  direction: decode
#  text: bcpf
#  shift: 2
#  Het resultaat zou moeten zijn: zand


# maak gebruik van deze code om je code hierboven aan te passen.
direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n")





