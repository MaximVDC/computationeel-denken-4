import unittest
import main


class InschrijvingenTest(unittest.TestCase):

    def test_dinsdag(self):
        self.assertEqual(main.convert_dag_into_day("dinsdag"), "tuesday")

    def test_donderdag(self):
        self.assertEqual(main.convert_dag_into_day("donderdag"), "thursday")

