import unittest
from unittest.mock import patch
import io
import main


class InschrijvingenTest(unittest.TestCase):

    def test_inschrijven_ingeschreven(self):
        self.assertEqual(main.inschrijven(["Jan", "Mieke", "Piet", "Geert"], "Geert"), "Deze persoon is reeds ingeschreven.")

    def test_inschrijven_niet_ingeschreven(self):
        self.assertEqual(main.inschrijven(["Jan", "Mieke", "Piet", "Geert"], "Fred"), "Fred is nu ingeschreven. In totaal zijn er 5 inschrijvingen.")

    def test_uitschrijven_ingeschreven(self):
        self.assertEqual(main.uitschrijven(["Jan", "Mieke", "Piet", "Geert"], "Geert"), ["Jan", "Mieke", "Piet"])

    def test_uitschrijven_niet_ingeschreven(self):
        self.assertEqual(main.uitschrijven(["Jan", "Mieke", "Piet", "Geert"], "Fred"), "Deze persoon stond niet op de gastenlijst.")