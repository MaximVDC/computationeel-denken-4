import unittest
from unittest.mock import patch
import io
import main


class VerhoogTest(unittest.TestCase):

    def test_verhoog(self):
        self.assertEqual(main.verhoog([1,2,8,9,7,-1,0]), [2,3,9,10,8,0,1])