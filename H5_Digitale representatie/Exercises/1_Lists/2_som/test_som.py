import unittest
from unittest.mock import patch
import io
import main


class SomTest(unittest.TestCase):

    def test_som(self):
        self.assertEqual(main.som([1,2,8,9,7,-1,0]), 26)