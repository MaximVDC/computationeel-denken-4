===================================================================================================================================
RANDOM MODULE
===================================================================================================================================

import random               importeer deze module bovenaan je code, indien je deze nodig hebt

random.randint(m, n)        retourneert een willekeurig getal tussen getal m en getal n [m,n]       bv. random.randint(2,6)         ->  4

random.shuffle(list)        retourneert een nieuwe lijst waarin de volgorde van de elementen        bv. random.shuffle([1,2,3])     ->  [2,3,1]
                            in list dooreen gegooid zijn.

random.choice(list)         retourneert een willekeurig element uit list                            bv. random.choice([8,9,6,4,7])  ->  4