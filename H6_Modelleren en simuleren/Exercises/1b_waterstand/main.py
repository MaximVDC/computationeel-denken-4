# We willen de waterstand (= attribuut) van een meer (= entiteit) onderzoeken. Elke dag kan het regenen of kan de zon
# schijnen, waardoor het meer zich vult of er water verdampt (= activiteiten). Aan het begin van elke dag bepalen we
# of we regen of zonneschijn mogen verwachten (= gebeurtenis). De hoeveelheid dagelijkse regen of verdamping (= variabelen)
# beïnvloeden de diepte van het meer. Op welke manier beïnvloedt een jaar regen of zon de waterstand van het meer?

# TODO-1: definieer een functie 'regen' die het aantal mm regen simuleert (min 1 mm, max 10 mm)


# TODO-2: definieer een functie 'zon' die het aantal mm regen dat verdampt,simuleert (min 1 mm, max 3 mm)


# TODO-3: definieer een functie 'vandaag_regen' die True teruggeeft als het vandaag regent, anders False



# TODO-4: definieer een functie 'vandaag_regen' die True teruggeeft als het vandaag regent, anders False




# TODO-5: Bekijk de afbeelding flowchart die de flowchart van het uiteindelijke algoritme uitbeeldt.
#  Definieer een functie 'simulation' die de simulatie uitvoert.

#waterstand meer op een bepaald moment
hoogte_meer = 2000
#lijst met alle dagelijkse waterstanden (365 voor een heel jaar)
meer = [2000]


# TODO-6: maak gebruik van de mode matplotlib en importeer de submodule pyplot. Aan het einde van 'simulation'
#  moet een grafiek getoond worden van de gesimuleerde gegevens met behulp van onderstaande code.
# pyplot.plot(range(365),meer)
# pyplot.show()