from tkinter import *
from tkinter import messagebox
from random import choice, randint, shuffle


# ---------------------------- PASSWORD GENERATOR ------------------------------- #

#Password Generator Project
def generate_password():
    letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']

    # TODO-1 selecteer willekeurig tussen 8-9 letters, selecteer willekeurig 2-3 symbolen en selecteer willekeurig 2-3 nummers.
    #  Sla deze op in aparte lijsten. Gebruik hiervoor de functie random.choice(lijst)



    # TODO-2 combineer deze geselecteerde tekens en shuffle ze door elkaar met de functie random.shuffle()



    # TODO-3 Maak een string van de geshufflede lijst met onderstaande code.


    # TODO-4 Voer deze code uit en test of je iedere keer een nieuw wachtwoord krijgt.



# ---------------------------- UI SETUP ------------------------------- #

window = Tk()
window.title("Password Manager")
window.config(padx=50, pady=50)

canvas = Canvas(height=200, width=200)
logo_img = PhotoImage(file="logo.png")
canvas.create_image(100, 100, image=logo_img)
canvas.grid(row=0, column=1)

#Labels
password_label = Label(text="Password:")
password_label.grid(row=3, column=0)

#Entries
password_entry = Entry(width=21)
password_entry.grid(row=3, column=1)

# Buttons
generate_password_button = Button(text="Generate Password", command=generate_password)
generate_password_button.grid(row=3, column=2)


window.mainloop()