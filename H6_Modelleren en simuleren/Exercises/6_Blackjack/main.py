
# Blackjack is een kaartspel dat vooral in casino's vaak (om geld) wordt gespeeld.
# Het doel van het spel is om de bank (dealer) te verslaan.
# Hierbij moet men proberen dichter bij de 21 punten te komen dan de bank.
# Als de speler boven de 21 punten uitkomt heeft hij verloren, ongeacht wat de bank heeft.
# Iedere speler krijgt in het begin, evenals de dealer zelf, twee kaarten, één dicht en één open.
# Alle spelers mogen in hun beurt kaarten vragen aan de bank (kopen) tot ze passen,
# een puntenaantal van 21 hebben of 'kapot' gaan.
# Hierbij moet de speler een zo hoog mogelijk puntenaantal onder de 21 of een 21 zien te krijgen.
# Als een speler aan het begin van de beurt een 10 en een aas heeft en dus al 21 punten heeft,
# dan hoeft deze speler niet om kaarten te vragen en is deze sowieso gewonnen.
#
# Kaartwaarden:
#   Heer 3 punten, vrouw 2 punten, boer 1 punt.
#   Aas is naar keuze 1 of 11 punten.
#   De kaarten 7 t/m 10 hebben hun normale puntwaarde, kaarten t/m 6 worden weggelaten.
#   De 'kleur' van de kaart is niet van belang.
#   De joker speelt niet mee.
#
# De bank is als laatst aan de beurt en laat zijn dichte kaart aan alle spelers zien zodra hij zijn beurt gaat uitspelen.
# Hierbij moet de bank volgens de volgende regels spelen:
#     Bij een handwaarde van 16 of lager moet de bank een kaart pakken;
#     Bij een handwaarde van 17 of hoger moet de bank passen.
# Als de bank is uitgespeeld eindigt de ronde. Alle spelers met een puntenaantal hoger dan die van de bank winnen.

# TODO-1: Bedenk een gepaste manier om de verschillende kaarten en hun waardes in het spel weer te geven. (lijst?, dictionary?, beide?)
#  Plaats deze eventueel in een andere python file die je hier dan importeert (meer leesbare code).




# TODO-2: Bedenk welke variabelen er sowieso nodig zullen zijn voor het spelverloop en declareer deze.




# TODO-3: definieer een functie draw_card() die een willekeurige! kaart trekt uit het deck kaarten (en deze dus ook verwijdert uit het deck kaarten).
#  Deze functie retourneert dan de getrokken kaart.




# TODO-4: Definieer een functie die de beginsituatie van het spel klaarzet (2 kaarten voor speler, 2 kaarten voor deler).
#   De kaarten die de speler kent, worden in de terminal geprint. Van de dealer worden enkel de zichtbare kaarten geprint.




# TODO-5: Zorg ervoor dat het spel nu volgens de spelregels verloopt tot een uiteindelijke winnaar gevonden is.
#  De winnaar wordt geprint in de terminal.


