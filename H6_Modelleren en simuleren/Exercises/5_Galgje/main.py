import random
from hangman_art import stages, logo
from hangman_words import word_list


# Voor deze opdracht proberen jullie het spel galgje te maken met de volgende vereisten:
#     - het spel begint met het tonen van het logo (staat al klaar)
#     - In hangman_art vind je de verschillende tekeningen die gebruikt worden tijdens het spel.
#               de variabele stages is een lijst van deze tekeneningen
#               len(stages) - 1 = het maximaal aantal levens de speler heeft
#               Wanneer het aantal levens == 0 is het game over en wordt er "Je hebt verloren." geprint
#     - De variabele word_list bevat een woordenlijst van mogelijke woorden die geraden moeten worden.
#     - De code random.choice( eigen lijst ) kan gebruikt worden om een willekeurig iets uit een lijst te kiezen.
#     - De lijst display kan gebruikt worden om de letters en strepen die op het speelscherm moeten komen op te slaan. Initieel staan er evenveel _ als
#       er letters in het te raden woord zitten.bvb. aapjes --> "______"
#       Je kan deze string printen met de volgende code: print(f"{' '.join(display)}")
#     - Wanneer een speler een letter invoert zijn er 3 verschillende opties:
#                   a) de letter was nog niet eerder gevraagd en zit in het woord   --> zorg ervoor dat de letter op de juiste plaatsen verschijnt
#                       bvb. __a__a_
#                   b) de letter werd eerder al gevraagd --> print "Je hebt deze letter al geraden"
#                   c) de letter zit niet in het woord --> de speler verliest een leven, een volgende tekening wordt getoond
#           Dit proces loopt door tot de speler alle letters geraden heeft of 0 levens heeft.
#       - Indien alle letters geraden werden --> print "Je wint."

print(logo)


display = []
