import datetime
import unittest
from unittest.mock import patch
import io
import main


class TimerTest(unittest.TestCase):

    @patch('builtins.input', side_effect=['5'])
    def test_countdown_from_10(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.countdown()
            self.assertEqual(fake_out.getvalue(), "5\n4\n3\n2\n1\n0\n")

    @patch('builtins.input', side_effect=['5'])
    def test_time_interval(self, mock_input):
        start_time = datetime.datetime.now()
        main.countdown()
        stop_time = datetime.datetime.now()
        time_diff = stop_time - start_time
        execution_time = time_diff.total_seconds()
        self.assertTrue(execution_time > 5 and execution_time < 7)


