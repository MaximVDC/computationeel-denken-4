import time

def countdown():
    # Herschrijf de timer uit de vorige oefening zodat deze dit keer gebruik maakt van een while-lus.


    # Deze oefening maakt duidelijk dat je een for of while lus altijd door elkaar kan gebruiken.
    # Het is niet dat de ene beter is dan de andere. In de ene situatie is een for lus nu eenmaal praktischer,
    # in een andere is de while lus nuttiger. Maak dus een verstandige keuze!

    time.sleep(1)

