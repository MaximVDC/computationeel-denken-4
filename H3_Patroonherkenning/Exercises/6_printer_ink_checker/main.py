def start_printer():
    # Maak een programma dat aan de gebruiker blijft vragen hoeveel pagina's deze wil printen tot de inkt op of bijna op is.
    # De volgende vereisten gelden:
    #       - je maakt gebruikt van een while-lus
    #       - wanneer de gebruiker meer dan het maximum aantal blz vraagt, print je "Gevraagd aantal bladzijden overschrijdt de maximale capaciteit van de inkt cartridge."
    #       - wanneer er nog voldoende inkt in de printer zit voor de aanvraag van de gebruiker, pas je de hoeveelheid beschikbare blz aan
    #               - indien er dan nog minder dan 10% van de maximale capaciteit overblijft, print je "Inkt cartridge moet binnenkort vervangen worden."
    #                   en stel je een vraag aan de gebruiker "Wil je de inkt cartridge vervangen?(y/n)" -> de gebruiker antwoordt met y ( = yes) of n ( = no)
    #       - wanneer de vraag van de gebruiker exact overeenkomt met de resterende capaciteit van de printer, print je "Inkt cartridge moet vervangen worden."
    #                 en vraag je opnieuw of de inkt cartridge vervangen moet worden.
    #       - als de vraag van de gebruiker groter is dan de resterende capaciteit van de printer, maar kleiner dan het maximum aantal pagina's,
    #         print je "Inkt cartridge moet vervangen worden voor deze opdracht."
    #         en vraag je opnieuw of de inkt cartridge moet vervangen worden.
    #       - Wanneer de gebruiker beslist om de cartridge te vervangen, stopt het programma en wordt de capaciteit van de printer gereset
    #         en print je "Nieuwe cartridge geïnstalleerd. x resterend." -> ipv x moet het nieuwe aantal resterende pagina's komen.
    MAX_PAGES = 100
    new_cartridge = 'n'

    # TODO-1: Schrijf hieronder je code (je kreeg al 2 variabelen om te starten)
    pass

























# Verwijder deze code niet.
start_printer()