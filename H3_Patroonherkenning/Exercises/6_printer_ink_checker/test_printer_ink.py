import unittest
from unittest.mock import patch
import io
import main

class RestDelingTest(unittest.TestCase):

    @patch('builtins.input', side_effect=['50', '41','y'])
    def test_printen_tot_onder_10_new_cartridge(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.start_printer()
            self.assertEqual(fake_out.getvalue(), "Inkt cartridge moet binnenkort vervangen worden.\nNieuwe cartridge geïnstalleerd. 100 resterend.\n")

    @patch('builtins.input', side_effect=['110','10','95','y'])
    def test_meer_dan_max_printen(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.start_printer()
            self.assertEqual(fake_out.getvalue(), "Gevraagd aantal bladzijden overschrijdt de maximale capaciteit van de inkt cartridge.\nInkt cartridge moet vervangen worden voor deze opdracht.\nNieuwe cartridge geïnstalleerd. 100 resterend.\n")

    @patch('builtins.input', side_effect=['100','y'])
    def test_exact_aantal_papier(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.start_printer()
            self.assertEqual(fake_out.getvalue(), "Inkt cartridge moet vervangen worden.\nNieuwe cartridge geïnstalleerd. 100 resterend.\n")