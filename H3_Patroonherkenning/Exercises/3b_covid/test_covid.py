import unittest
from unittest.mock import patch
import io
import main

class CovidTest(unittest.TestCase):
    @patch('builtins.input', side_effect=['W', 'B', 'Z', 'B', 'B', 'W', 'Z', 'B', 'X'])
    def test_normal(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.count_british_variants()
            self.assertEqual(fake_out.getvalue(), "W\nB\nZ\nB\nB\nW\nZ\nB\n4\n")

    @patch('builtins.input', side_effect=['W', 'B', 'L', 'B', 'B', 'W', 'Z', 'B', 'X'])
    def test_wrong_input(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.count_british_variants()
            self.assertEqual(fake_out.getvalue(), "WBBBWZB\n4\n")