import unittest
from unittest.mock import patch
import io
import main

class RestDelingTest(unittest.TestCase):

    @patch('builtins.input', side_effect=['17', '4'])
    def test_delen_met_rest(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.rest_bepalen()
            self.assertEqual(fake_out.getvalue(), "De rest is 1.\n")

    @patch('builtins.input', side_effect=['17', '0'])
    def test_delen_door_0(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.rest_bepalen()
            self.assertEqual(fake_out.getvalue(), "Delen door 0 is niet gedefinieerd.\n")

    @patch('builtins.input', side_effect=['8', '4'])
    def test_opgaande_deling(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.rest_bepalen()
            self.assertEqual(fake_out.getvalue(), "De rest is 0.\n")