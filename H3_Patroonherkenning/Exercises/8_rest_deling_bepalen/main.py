def rest_bepalen():
    # Het is jouw taak om een programma te schrijven dat voor een door de gebruiker ingevoerd deeltal en deler de rest
    # kan bepalen van deze deling. Je mag hiervoor NIET de modulo (%) bewerking gebruiken!.
    # Beschouw de volgende bewerking:
    # 15 / 3 = 5    Hier is 15 het deeltal (D), 3 de deler (d) en 5 het quotiënt (q) en 0 is de rest (r).
    # Houd ook rekening met de volgende wiskunde formule (herschrijf deze desnoods in een meer bruikbare vorm):
    #       D = ( q * d ) + r           17 = ( 4 * 4 ) + 1      -> 17 / 4 -> rest = 1

    # HINT: maak voor elk van de elementen uit de bovenstaande formule een variabele.
    #       Om de rest te vinden moet je het quotiënt blijven vergroten totdat ??? (denk zelf eens wiskundig na met een eenvoudig voorbeeld)
    #           bvb. deeltal 17 en deler 4 -> rest = 1

    # Belangrijk!
    #   Vraag de gebruiker eerst om het deeltal, daarna pas om de deler.
    #   Print de restwaarde precies zoals in het volgende voorbeeld: "De rest is 15."
    #   Aangezien het onmogelijk is om te delen door 0 moet er in dit geval geprint worden: "Delen door 0 is niet gedefinieerd."

    pass
