import unittest
from unittest.mock import patch
import io
import main

class ElfjeTest(unittest.TestCase):

    @patch('builtins.input', return_value='Everyone knows this is a forced attempt to create an elfje.')
    def test_elfje(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.check_if_elfje()
            self.assertEqual(fake_out.getvalue(), "Dit is een elfje.\n")

    @patch('builtins.input', return_value='This is not an elfje.')
    def test_not_elfje(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.check_if_elfje()
            self.assertEqual(fake_out.getvalue(), "Dit gedicht bevat 5 woorden. Het is dus geen elfje.\n")
