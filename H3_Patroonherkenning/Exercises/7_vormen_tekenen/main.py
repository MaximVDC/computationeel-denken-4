import turtle as t
# Jullie blijven van deze code af.
pen = t.Turtle()
pen.color("Blue")

# Voor deze opdracht maken jullie een wiskundige tekening. Zie het bijgeleverde filmpje om te zien wat de bedoeling is.
# Jullie tekenen een driehoek, daarna een vierhoek, daarna een vijfhoek, ... tot en met een 10-hoek.
# Jullie hoeven je geen zorgen te maken over het scherm en alle technische details.
# Om het potlood te bewegen over het scherm maken jullie gebruik van de volgende code snippets:
#       pen.forward(100)    vooruit bewegen
#       pen.right(angle)    het getal dat je invult ipv angle bepaalt de hoek waarover naar rechts gedraaid wordt.
# HINT: De som van de hoeken waarover je moet draaien voor elke verschillende veelhoek moet steeds gelijk zijn aan 360.
#       Bijvoorbeeld: voor een driehoek 360 / 3 = 120, voor een vierhoek 360 / 4 = 90, ...
# Deze code test je door te kijken of het programma daadwerkelijk tekent wat gevraagd werd.

# Programmeer hieronder jullie oplossing:

