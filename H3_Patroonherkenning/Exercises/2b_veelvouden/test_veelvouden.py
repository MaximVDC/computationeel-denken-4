import unittest
from unittest.mock import patch
import io
import main


class VeelvoudenTest(unittest.TestCase):

    @patch('builtins.input', side_effect=['9', '28', '8'])
    def test_forward_forward(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.calc_multiples()
            self.assertEqual("16\n24\n", fake_out.getvalue())
    