import unittest
from unittest.mock import patch
import io
import main


class SchaakclubTest(unittest.TestCase):

    @patch('builtins.input', side_effect=['^', '^'])
    def test_forward_forward(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.kruispunt()
            self.assertEqual(fake_out.getvalue(), "Dansclub bereikt\n")

    @patch('builtins.input', side_effect=['>', '^'])
    def test_right_forward(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.kruispunt()
            self.assertEqual(fake_out.getvalue(), "Verloren gereden\n")

    @patch('builtins.input', side_effect=['<', '^'])
    def test_left_forward(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.kruispunt()
            self.assertEqual(fake_out.getvalue(), "Verloren gereden\n")

    @patch('builtins.input', side_effect=['^', '<'])
    def test_forward_left(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.kruispunt()
            self.assertEqual(fake_out.getvalue(), "Verloren gereden\n")

    @patch('builtins.input', side_effect=['>', '>'])
    def test_right_right(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.kruispunt()
            self.assertEqual(fake_out.getvalue(), "Verloren gereden\n")

    @patch('builtins.input', side_effect=['^', '>'])
    def test_forward_right(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.kruispunt()
            self.assertEqual(fake_out.getvalue(), "Verloren gereden\n")

    @patch('builtins.input', side_effect=['<', '>'])
    def test_left_right(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.kruispunt()
            self.assertEqual(fake_out.getvalue(), "Verloren gereden\n")

    @patch('builtins.input', side_effect=['<', '<'])
    def test_left_left(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.kruispunt()
            self.assertEqual(fake_out.getvalue(), "Verloren gereden\n")

    @patch('builtins.input', side_effect=['>', '<'])
    def test_right_left(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.kruispunt()
            self.assertEqual(fake_out.getvalue(), "Schaakclub bereikt\n")


if __name__ == '__main__':
    unittest.main()
