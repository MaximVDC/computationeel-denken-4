def kruispunt():

    kruispunt_1 = input("Richting aan eerste kruispunt: ")
    kruispunt_2 = input("Richting aan tweede kruispunt: ")

    if kruispunt_1 == "^" or kruispunt_2 == "^":
        print("Dansclub bereikt")
    elif kruispunt_1 == ">" or kruispunt_2 == "<":
        print("Schaakclub bereikt")
    else:
        print("Verloren gereden")



# Zet onderstaande lijn in commentaar wanneer je wil testen of je de oefening correct hebt opgelost
kruispunt()