import unittest
from unittest.mock import patch
import io
import main

class SchrijnwerkerTest(unittest.TestCase):

    @patch('builtins.input', side_effect=['eik', '4'])
    def test_eik(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.calc_nails()
            self.assertEqual(fake_out.getvalue(), "Je hebt 3200 nagels nodig.\n")

    @patch('builtins.input', side_effect=['den', '6'])
    def test_den(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.calc_nails()
            self.assertEqual(fake_out.getvalue(), "Je hebt 2880 nagels nodig.\n")

    @patch('builtins.input', side_effect=['beuk', '2'])
    def test_beuk(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.calc_nails()
            self.assertEqual(fake_out.getvalue(), "Je hebt 1480 nagels nodig.\n")