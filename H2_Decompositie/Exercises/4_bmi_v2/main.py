# 1_bmi schalen:
#
# Waarde              Gewichtstoestand
#
# < 18.5              ondergewicht
# 18.5 - 24.9         normaal gewicht
# 25 - 29.9           overgewicht
# 30 - 34.9           matige obesitas
# 35 - 39.9           ernstige obesitas
# > 40                morbide obesitas

def bmi2():
    # TODO-1: kopieer of maak de 1_bmi-oefening uit hoofdstuk 1 (It_1_algoritme)
    # TODO-2: afhankelijk van de 1_bmi-score toon je naast de waarde ook de gewichtstoestand via print()

    pass

    # Print volgende zin "Jouw 1_bmi is # en hierdoor is jouw gewichtstoestand #."
    # Vervang de # door de gebruikte variabele

    # HINT: maak gebruik van de volgende functie om je 1_bmi waarde af te ronden op 1 getal na de komma
    #       round(getal, 1)

















# Zet onderstaande lijn in commentaar wanneer je wil testen of je de oefening correct hebt opgelost
bmi2()