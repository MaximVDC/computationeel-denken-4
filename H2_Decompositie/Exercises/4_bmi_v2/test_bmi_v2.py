import unittest
from unittest.mock import patch
import io
import main


class TimerTest(unittest.TestCase):

    @patch('builtins.input', side_effect=['60', '2'])
    def test_ondergewicht(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.bmi2()
            self.assertEqual(fake_out.getvalue(),
                             "Jouw bmi is 15.0 en hierdoor is jouw gewichtstoestand ondergewicht.\n")

    @patch('builtins.input', side_effect=['80', '1.8'])
    def test_normaal_gewicht(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.bmi2()
            self.assertEqual(fake_out.getvalue(), "Jouw bmi is 24.7 en hierdoor is jouw gewichtstoestand normaal gewicht.\n")

    @patch('builtins.input', side_effect=['100', '1.8'])
    def test_matige_obesitas(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.bmi2()
            self.assertEqual(fake_out.getvalue(), "Jouw bmi is 30.9 en hierdoor is jouw gewichtstoestand matige obesitas.\n")

    @patch('builtins.input', side_effect=['150', '1.8'])
    def test_morbide_obesitas(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.bmi2()
            self.assertEqual(fake_out.getvalue(), "Jouw bmi is 46.3 en hierdoor is jouw gewichtstoestand morbide obesitas.\n")

    @patch('builtins.input', side_effect=['120', '1.8'])
    def test_ernstige_obesitas(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.bmi2()
            self.assertEqual(fake_out.getvalue(), "Jouw bmi is 37.0 en hierdoor is jouw gewichtstoestand ernstige obesitas.\n")