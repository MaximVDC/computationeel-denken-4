import unittest
from unittest.mock import patch
import io
import main
class driehoekTest(unittest.TestCase):

    @patch('builtins.input', side_effect=['33','57'])
    def test_driehoek_1(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.solve_triangle()
            self.assertEqual(fake_out.getvalue(), '90\n')

    @patch('builtins.input', side_effect=['49','82'])
    def test_driehoek_2(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.solve_triangle()
            self.assertEqual(fake_out.getvalue(), '49\n')

    @patch('builtins.input', side_effect=['90','49'])
    def test_driehoek_3(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.solve_triangle()
            self.assertEqual(fake_out.getvalue(), '41\n')

    @patch('builtins.input', side_effect=['91','100'])
    def test_driehoek_stomp(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.solve_triangle()
            self.assertEqual(fake_out.getvalue(), 'onmogelijke driehoek\n')