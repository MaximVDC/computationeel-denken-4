import unittest
from unittest.mock import patch
import io
import main


class TimerTest(unittest.TestCase):

    @patch('builtins.input', side_effect=['100'])
    def test_100(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.onderscheiding()
            self.assertEqual(fake_out.getvalue(),
                             "Je behaalde 100% en bent hierdoor geslaagd met grootste onderscheiding en de gelukwensen van de examencommissie.\n")
    @patch('builtins.input', side_effect=['90'])
    def test_90(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.onderscheiding()
            self.assertEqual(fake_out.getvalue(),
                             "Je behaalde 90% en bent hierdoor geslaagd met grootste onderscheiding en de gelukwensen van de examencommissie.\n")
    @patch('builtins.input', side_effect=['85'])
    def test_grootste_onderscheiding(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.onderscheiding()
            self.assertEqual(fake_out.getvalue(),
                             "Je behaalde 85% en bent hierdoor geslaagd met grootste onderscheiding.\n")

    @patch('builtins.input', side_effect=['77'])
    def test_grote_onderscheiding(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.onderscheiding()
            self.assertEqual(fake_out.getvalue(),
                             "Je behaalde 77% en bent hierdoor geslaagd met grote onderscheiding.\n")
    @patch('builtins.input', side_effect=['68'])
    def test_onderscheiding(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.onderscheiding()
            self.assertEqual(fake_out.getvalue(),
                             "Je behaalde 68% en bent hierdoor geslaagd met onderscheiding.\n")

    @patch('builtins.input', side_effect=['50'])
    def test_voldoende(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.onderscheiding()
            self.assertEqual(fake_out.getvalue(),
                             "Je behaalde 50% en bent hierdoor geslaagd op voldoende wijze.\n")

    @patch('builtins.input', side_effect=['40'])
    def test_niet_geslaagd(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.onderscheiding()
            self.assertEqual(fake_out.getvalue(),
                             "Je behaalde 40% en bent hierdoor niet geslaagd.\n")