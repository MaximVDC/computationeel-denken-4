import unittest
from unittest.mock import patch
import io
import main


class CrossTest(unittest.TestCase):

    @patch('builtins.input', side_effect=['500', '1'])
    def test_1_stuk(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.cross_selling()
            self.assertEqual(fake_out.getvalue(),
                             "U kocht 1 stuks en krijgt hierdoor 10% korting.\nHierdoor komt het totale te betalen bedrag op 450.0 euro.\n")
    @patch('builtins.input', side_effect=['500', '2'])
    def test_2_stuk(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.cross_selling()
            self.assertEqual(fake_out.getvalue(),
                             "U kocht 2 stuks en krijgt hierdoor 20% korting.\nHierdoor komt het totale te betalen bedrag op 400.0 euro.\n")
    @patch('builtins.input', side_effect=['500', '3'])
    def test_3_stuk(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.cross_selling()
            self.assertEqual(fake_out.getvalue(),
                             "U kocht 3 stuks en krijgt hierdoor 30% korting.\nHierdoor komt het totale te betalen bedrag op 350.0 euro.\n")
    @patch('builtins.input', side_effect=['500', '4'])
    def test_4_stuk(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.cross_selling()
            self.assertEqual(fake_out.getvalue(),
                             "U kocht 4 stuks en krijgt hierdoor 30% korting.\nHierdoor komt het totale te betalen bedrag op 350.0 euro.\n")
    @patch('builtins.input', side_effect=['500', '5'])
    def test_5_stuk(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.cross_selling()
            self.assertEqual(fake_out.getvalue(),
                             "U kocht 5 stuks en krijgt hierdoor 50% korting.\nHierdoor komt het totale te betalen bedrag op 250.0 euro.\n")

    @patch('builtins.input', side_effect=['500', '6'])
    def test_6_stuk(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.cross_selling()
            self.assertEqual(fake_out.getvalue(),
                             "U kocht 6 stuks en krijgt hierdoor 50% korting.\nHierdoor komt het totale te betalen bedrag op 250.0 euro.\n")