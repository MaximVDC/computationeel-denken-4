import unittest
import main

class ToegangsprijsTest(unittest.TestCase):

    def test_normal_price(self):
        self.assertEqual(main.toegangsprijs(2, 2), 132)

    def test_6_person_price(self):
        self.assertEqual(main.toegangsprijs(3, 3), 186)

    def test_20_person_price(self):
        self.assertEqual(main.toegangsprijs(10, 10), 510)
        