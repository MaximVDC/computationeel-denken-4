import unittest
import main

class VolumeTest(unittest.TestCase):

    def test_volume(self):
        self.assertEqual(m.volume(25, 4), 100)