import unittest
import main


class StoppenTest(unittest.TestCase):

    def test_spoorweg(self):
        self.assertEqual(main.spoorweg("rood"), "stoppen")
        self.assertEqual(main.spoorweg("groen"), "doorrijden")

    def test_stoppen(self):
        self.assertEqual(main.stoppen("rood"), True)
        self.assertEqual(main.stoppen("groen"), False)