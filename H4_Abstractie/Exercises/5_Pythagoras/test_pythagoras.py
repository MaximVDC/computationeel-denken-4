import unittest
from unittest.mock import patch
import io
import main


class PythagorasTest(unittest.TestCase):

    def test_pythagoras(self):
        self.assertEqual(main.pythagoras(2,3), 3.605551275463989)