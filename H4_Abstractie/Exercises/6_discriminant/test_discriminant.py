import unittest
from unittest.mock import patch
import io
import main


class DiscriminantTest(unittest.TestCase):

    def test_determinant(self):
        self.assertEqual(main.discriminant(1,4,2), 8)