import unittest
import main

class KortingTest(unittest.TestCase):

    def test_korting(self):
        self.assertEqual(main.korting(25), 2.5)

    def test_korting2(self):
        self.assertEqual(main.korting(25,0.2), 5)