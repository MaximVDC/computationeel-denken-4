tekst = "EEN willekeurige TEKST"
getal = -3.14159265359
# TODO-1: zet de variabele tekst om naar hoofdletters

# TODO-2: zet de variabele tekst om naar kleine letters

# TODO-3: laat python tellen uit hoeveel tekens de variabele tekst bestaat

# TODO-4: rond de variabele getal af op 3 cijfers na de komma

# TODO-5: geef de absolute waarde van de variabele getal

# TODO-6: bereken de vierkantswortel van de variabele getal

# TODO-7: verhef de variabele getal tot de 3de macht