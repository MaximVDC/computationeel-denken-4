# Gebruik de functie input() om gegevens te laten invoeren door de gebruiker
# Maak gebruik van de methode print() om te controleren of de oefening gelukt is


def convert_cm_to_m():
    pass
    # TODO-1: Vraag aan de gebruiker om zijn lengte in te voeren in cm en zet deze om naar meter




    # TODO-2: Print volgende zin "Je bent # meter groot." waarin je het resultaat van bovenstaande TODO gebruikt
    # TODO-2: Vervang de # door de gebruikte variabele




convert_cm_to_m()



def convert_kg_to_lb():
    pass
    # TODO-1: Vraag aan de gebruiker om zijn gewicht in te voeren in kg en zet deze om naar pounds (lb)
    # 1 kilogram = 2.20462262 pounds




    # TODO-2: Print volgende zin "Jouw gewicht in pounds is # lb." waarin je het resultaat van bovenstaande TODO gebruikt
    # TODO-2: Vervang de # door de gebruikte variabele




convert_kg_to_lb()


#TODO-Extra:

def convert_m_to_ft_inch():
    pass
    # TODO-1: Vraag aan de gebruiker zijn lengte in meter en zet deze om naar feet & inches
    # 1 meter = 3.2808399 feet 1 meter = 39.3700787 inches 1 foot = i # 12 inches





    # TODO-2: Print volgende zin "Jouw lengte is # feet en # inch." waarin je het resultaat van bovenstaande TODO gebruikt
    # TODO-2: Vervang de # door de gebruikte variabelen



convert_m_to_ft_inch()