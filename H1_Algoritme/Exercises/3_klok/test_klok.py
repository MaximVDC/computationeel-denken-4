
import unittest
from unittest.mock import patch
import io
import main


class TimerTest(unittest.TestCase):

    @patch('builtins.input', side_effect=['15236'])
    def test_countdown_from_10(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.clock()
            self.assertEqual(fake_out.getvalue(), "Je gaf 15236 minuten in.\nDit is 253 uren en 56 minuten.\n")