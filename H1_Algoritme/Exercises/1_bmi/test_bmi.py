import unittest
from unittest.mock import patch
import io
import main


class TimerTest(unittest.TestCase):

    @patch('builtins.input', side_effect=['80','2'])
    def test_countdown_from_10(self, mock_input):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main.bmi()
            self.assertEqual(fake_out.getvalue(), "Jouw bmi is 20.0.\n")